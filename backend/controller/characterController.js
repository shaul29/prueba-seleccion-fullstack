import asyncHandler from 'express-async-handler'
import Character from '../models/characterModel.js'

// @desc    Fetch all characters
// @route   GET /api/character
// @access  Public
const getCharacters = asyncHandler(async (req, res) => {
    const pageSize = 10
    const page = Number(req.query.pageNumber) || 1

    const keyword = req.query.keyword
        ? {
            name: {
                $regex: req.query.keyword,
                $options: 'i',
            },
        }
        : {}

    const count = await Character.countDocuments({ ...keyword })
    const characters = await Character.find({ ...keyword })
        .limit(pageSize)
        .skip(pageSize * (page - 1))

    res.json({ characters, page, pages: Math.ceil(count / pageSize) })
})



// @desc    Fetch single character
// @route   GET /api/characters/:id
// @access  Public
const getCharacterById = asyncHandler(async (req, res) => {
    const character = await Character.findById(req.params.id)

    if (character) {
        res.json(character)
    } else {
        res.status(404)
        throw new Error('character not found...')
    }
})

export {
    getCharacters,
    getCharacterById
}

