import express from 'express'
const router = express.Router()
import {
    getCharacters,
    getCharacterById
} from '../controller/characterController.js'

router.route('/').get(getCharacters)

router.route('/:id').get(getCharacterById)

export default router