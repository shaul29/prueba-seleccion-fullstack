import mongoose from 'mongoose'

const characterSchema = mongoose.Schema({
    titles: [{
        type: String
    }],
    spouse: [{
        type: String
    }],
    children: [{
        type: String
    }],
    allegiance: [{
        type: String
    }],
    books: [{
        type: String
    }],
    plod: {
        type: Number
    },
    longevity: [{
        type: String
    }],
    plodB: {
        type: Number
    },
    plodC: {
        type: Number
    },
    longevityB: [{
        type: String
    }],
    longevityC: [{
        type: String
    }],
    _id: {
        type: String
    },
    name: {
        type: String
    },
    slug: {
        type: String
    },
    gender: {
        type: String
    },
    culture: {
        type: String,
    },
    house: {
        type: String,
    },
    alive: {
        type: Boolean,
    },
    createdAt: {
        type: Date,
    },
    updatedAt: {
        type: Date
    },
    __v: {
        type: Number
    },
    pagerank: {
        title: { type: String },
        rank: { type: Number }
    },
    id: {
        type: String
    }
})

const Character = mongoose.model('Character', characterSchema)

export default Character