import React, { useEffect } from 'react'
import { Fragment } from 'react';
import { Card } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from "react-router-dom";
import { listCharacterDetails } from '../actions/characterActions';
import Loader from '../components/Loader';
import Message from '../components/Message';

const CharacterScreen = () => {
    const { id } = useParams()

    const dispatch = useDispatch()

  const characterDetails = useSelector((state) => state.characterDetails)
  const { error, character, loading} = characterDetails

  useEffect(() => {
    dispatch(listCharacterDetails(id))
  }, [dispatch,id])

  if (loading) {
      return <Loader />
  }
    return (
        <>
            {error && <Message variant='danger'>{error}</Message>}
            {Array.isArray(character.books) && 
            <Card style={{ width: '60rem' }}>
  <Card.Body>
    <Card.Title>{character.name}</Card.Title>
    <Card.Subtitle className="mb-2 text-muted">{`Gender: ${character.gender}`}</Card.Subtitle>
    <Card.Subtitle className="mb-2 text-muted">{`Titles: ${character.titles}`}</Card.Subtitle>
    <Card.Subtitle className="mb-2 text-muted">{`Slug: ${character.slug}`}</Card.Subtitle>
    <Card.Subtitle className="mb-2 text-muted">{`House: ${character.house}`}</Card.Subtitle>
    <Card.Subtitle className="mb-2 text-muted">{`Rank: ${character.pagerank.rank}`}</Card.Subtitle>
    <Card.Subtitle className="mb-2 ">Books</Card.Subtitle>
    <ul>
          {character.books.map((book) => (
            <li>{book}</li>
              ))}
    </ul>
  </Card.Body>
</Card>}
        </>
    )
}


export default CharacterScreen
