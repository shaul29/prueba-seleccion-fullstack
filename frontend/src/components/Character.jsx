import React from 'react'
import { Link } from 'react-router-dom'
import { Card } from 'react-bootstrap'


const Character = ({ character }) => {
  return (
    <Card className='my-3 p-3 rounded'>
      <Link to={`/character/${character._id}`}>
        <h4>{character.name}</h4>
      </Link>
      <Card.Body>
          <Card.Title as='div'>
            <strong>{`Genero: ${character.gender}`}</strong>
          </Card.Title>

        <Card.Text as='div'>
        <strong>{`Slug: ${character.slug}`}</strong>
        </Card.Text>
      </Card.Body>
    </Card>
  )
}

export default Character