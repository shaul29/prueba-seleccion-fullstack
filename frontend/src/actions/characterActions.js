import {
    CHARACTER_DETAILS_FAIL,
    CHARACTER_DETAILS_REQUEST,
    CHARACTER_DETAILS_SUCCESS,
    CHARACTER_LIST_FAIL,
    CHARACTER_LIST_REQUEST,
    CHARACTER_LIST_SUCCESS
} from "../constants/characterConstants"

import axios from 'axios'

export const listCharacters = (keyword = '', pageNumber = '') => async (
    dispatch
) => {
    try {
        dispatch({ type: CHARACTER_LIST_REQUEST })

        const { data } = await axios.get(
            `/api/characters?keyword=${keyword}&pageNumber=${pageNumber}`
        )

        dispatch({
            type: CHARACTER_LIST_SUCCESS,
            payload: data,
        })
    } catch (error) {
        dispatch({
            type: CHARACTER_LIST_FAIL,
            payload:
                error.response && error.response.data.message
                    ? error.response.data.message
                    : error.message,
        })
    }
}

export const listCharacterDetails = (id) => async (dispatch) => {
    try {
        dispatch({ type: CHARACTER_DETAILS_REQUEST })

        const { data } = await axios.get(`/api/characters/${id}`)

        dispatch({
            type: CHARACTER_DETAILS_SUCCESS,
            payload: data,
        })
    } catch (error) {
        dispatch({
            type: CHARACTER_DETAILS_FAIL,
            payload:
                error.response && error.response.data.message
                    ? error.response.data.message
                    : error.message,
        })
    }
}

